export function Header() {
  return (
    <header className="bg-gray-700 text-white py-4">
      <div className="container mx-auto flex justify-between items-center">
        <div className="flex items-center">
          <img className="h-6 mr-2 rounded" src="/logo.png" alt="Logo" />
          <h1 className="text-lg font-semibold">Mebious</h1>
        </div>
        <nav>
          <ul className="flex space-x-4">
            <li>
              <a className="hover:text-gray-300" href="/random">Random</a>
            </li>
            <li>
              <a className="hover:text-gray-300" href="/search">Search</a>
            </li>
          </ul>
        </nav>
      </div>
    </header>
  );
}

// export default Header;
// import { FaBars, FaTimes } from "react-icons/fa";
// import { useRef } from "preact/compat";


// export function Header() {
//   const navRef = useRef();

//   const showNavbar = () => {
//     navRef.current.classList.toggle("res_nav")
//   }

//   return (
//     <header className="bg-yellow-400 text-white py-4">
//       <div className="container mx-auto flex justify-between items-center">
//         <div className="flex items-center">
//           <img className="h-6 mr-2 rounded" src="/logo.png" alt="Logo" />
//           <h1 className="text-lg font-semibold">Mebious</h1>
//         </div>
//         <nav ref={navRef}>
//           <ul className="flex space-x-4">
//             <li>
//               <a className="hover:text-gray-300" href="/search">Home</a>
//             </li>
//             <li>
//               <a className="hover:text-gray-300" href="/search">About Us</a>
//             </li>
//             <li>
//               <a className="hover:text-gray-300" href="#">Random</a>
//             </li>
//             <li>
//               <a className="hover:text-gray-300" href="/search">Search</a>
//             </li>
//             <button className="nav-btn nav-close-btn" onClick={showNavbar}>
//               <FaTimes />
//             </button>
//           </ul>

//         </nav>
//         <button className="nav-btn" onClick={showNavbar}>
//           <FaBars />
//         </button>
//       </div>
//     </header>
//   );
// }

export default Header;
